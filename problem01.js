document.getElementById('p1btn1').addEventListener('click', () => problem01());

const problem01 = () => {
    const arraySize = parseInt(prompt('Enter Array Size'));

    const arrayElements = [];

    for (let i = arraySize; i > 0; i--) {
        arrayElements.push(parseInt(prompt(`Enter Number: ${arraySize - i + 1}`)));
    }

    let positiveNumber = 0;
    let nagativeNumber = 0;
    let zeroNumber = 0;
    for (let i = arrayElements.length; i > 0; i--) {
        if (arrayElements[(i - 1)] > 0) {
            positiveNumber++;
        } else if (arrayElements[(i - 1)] < 0) {
            nagativeNumber++;
        } else if (arrayElements[(i - 1)] === 0) {
            zeroNumber++;
        }
    }

    // let x = 63.36727377232;

    // console.log(x.toFixed(6));

    let pratio = (positiveNumber / arraySize).toFixed(6);
    let nratio = (nagativeNumber / arraySize).toFixed(6);
    let zratio = (zeroNumber / arraySize).toFixed(6);

    document.getElementById('answer1').innerHTML = `<ul>
    <li>Array are : ${arrayElements}</li>
    <li>Positive number : ${positiveNumber}</li>
    <li>Positive ratio : ${pratio}</li>
    <li>Nagative number : ${nagativeNumber}</li>
    <li>Nagative ratio : ${nratio}</li>
    <li>Zero number : ${zeroNumber}</li>
    <li>Zero ratio : ${zratio}</li>
    </ul>`

    console.log(arrayElements, positiveNumber, nagativeNumber, zeroNumber, pratio, nratio, zratio);
}

