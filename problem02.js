document.getElementById('p2btn2').addEventListener('click', () => problem02());

const problem02 = () => {
    const matrixSize = parseInt(prompt('Enter Matrix Size'));
    const numberOfElement = matrixSize * matrixSize;

    const matrixElements = [];
    const leftDiaElements = [];
    const rightDiaElements = [];

    for (let i = numberOfElement; i > 0; i--) {
        matrixElements.push(parseInt(prompt(`Enter Element Number: ${numberOfElement - i + 1}`)));
    }

    for (let i = matrixSize - 1; i >= 0; i--) {
        let rden = ((i * matrixSize) + (i));
        leftDiaElements.push(matrixElements[rden]);
    }

    for (let i = 0; i < matrixSize; i++) {
        let rden = (((i + 1) * matrixSize) - (i));
        rightDiaElements.push(matrixElements[rden - 1]);
    }

    let sumLeft = 0;
    for (let i = leftDiaElements.length; i > 0; i--) {
        sumLeft += leftDiaElements[(i - 1)];
    }
    let sumRight = 0;
    for (let i = rightDiaElements.length; i > 0; i--) {
        sumRight += rightDiaElements[(i - 1)];
    }

    let ans = Math.abs(sumLeft - sumRight);
    document.getElementById('answer2').innerText = `Answer: ${ans}`

    console.log(matrixElements, sumLeft, sumRight, ans);
}

const sumFoo = (x, y) => {
    let total = 0;
    for (let i = x; i > 0; i--) {
        total += y[i - 1];
    }
    console.log(total);
}